package com.vpnmastersm.singaporevpnmaster;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class NavActivity extends AppCompatActivity implements View.OnClickListener {
    private AlertDialog AboutDialog, RateAppDialog;
    LinearLayout rateus, share, privacy, aboutus, feedBack;
    ConstraintLayout iv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav2);
        rateus = findViewById(R.id.rateus);
        share = findViewById(R.id.share);
        privacy = findViewById(R.id.privacy);
        aboutus = findViewById(R.id.aboutus);
        feedBack = findViewById(R.id.feedBack);
        iv_back = findViewById(R.id.iv_back);
        rateus.setOnClickListener(this);
        share.setOnClickListener(this);
        privacy.setOnClickListener(this);
        aboutus.setOnClickListener(this);
        feedBack.setOnClickListener(this);
        iv_back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.aboutus:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("About Us");
                builder.setMessage("VPN Master Team  by SMkoki");
                builder.setPositiveButton("Ok", (dialog, which) -> dialog.dismiss());

                builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
                AboutDialog = builder.create();
                AboutDialog.show();
                break;
            case R.id.rateus:
                RateAppDialog();
                // Toast.makeText(this, "url needed", Toast.LENGTH_SHORT).show();

                break;


            case R.id.share:

                final String shareurl = "https://play.google.com/store/apps/details?id=com.vpnmastersm.singaporevpn";


                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                String sharebody = "Singapore VPN - Free & Unlimited VPN for Android, with the Best VPN service and Fastest speed. \n" +
                        "\n" +
                        "https://play.google.com/store/apps/details?id=com.vpnmastersm.singaporevpn\n";
                sharingIntent.putExtra(Intent.EXTRA_TEXT, sharebody);
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, shareurl);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
//                Toast.makeText(this, "url needed", Toast.LENGTH_SHORT).show();


                break;

            case R.id.privacy:

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://smkoki2020.blogspot.com/2020/10/vpn-master-team.html")));
                break;

            case R.id.feedBack:
                Intent intent = new Intent(this, FeedbackActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.iv_back:
                finish();
                break;
        }

    }

    private void RateAppDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Rate Us");
        builder.setMessage("Please take a moment to Rate our Application");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.vpnmastersm.singaporevpn")));

            }
        });

        builder.setNegativeButton("Later", (dialog, which) -> dialog.dismiss());


        RateAppDialog = builder.create();
        RateAppDialog.show();

    }

}