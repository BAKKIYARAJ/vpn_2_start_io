package com.vpnmastersm.singaporevpnmaster;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashScreenActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        new Thread(() -> {
            doWork();
            startApp();
            finish();
        }).start();
    }

    private void doWork() {
        for (int progress = 0; progress < 60; progress += 10) {
            try {
                Thread.sleep(200);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startApp() {
        Intent intent = new Intent(SplashScreenActivity.this, ActivityVpn.class);
        startActivity(intent);
    }
}
